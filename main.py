from text import *


txt = ""
output = ""

print("Reading data from the webpage ...")
raw_html = simple_get('https://www.anandabazar.com/others/science/chandrayaan-2-s-vikam-had-hard-landing-stated-by-centre-dgtl-1.1073535')
soup = BeautifulSoup(raw_html, 'html.parser')

heading = soup.find('div', attrs={'id': 'story_container'}).find('h1')
txt += heading.text

for el in soup.find('div', attrs={'class': 'articleBody'}).find_all('p'):
	txt += " " + el.text

txt = prepare_string(txt)
	
with open('words.txt', 'w', encoding='utf-8') as f_out:
	for x in txt.split():
		f_out.write(x)
		f_out.write("\n")
print("All words are stored successfully !")

links = getLinks("https://www.anandabazar.com/sitemap")
print(links)

